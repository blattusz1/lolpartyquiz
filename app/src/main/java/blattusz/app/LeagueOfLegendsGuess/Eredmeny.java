package blattusz.app.LeagueOfLegendsGuess;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.content.Intent;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;

import static blattusz.app.LeagueOfLegendsGuess.MainActivity.Elrontott;
import static blattusz.app.LeagueOfLegendsGuess.MainActivity.Eltalat;


public class Eredmeny extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        System.gc();


        setContentView(R.layout.eredmeny);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        TextView tvSSID1 = (TextView) findViewById(R.id.score);
        tvSSID1.setText(""+(Eltalat.size()-2)+"p");






        Button bn2;
        bn2 = (Button)findViewById(R.id.NextPlayer);
        bn2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                CategoryMenu.GlobalSec = CategoryMenu.NewGameGlobalSec;
               Intent i=new Intent(Eredmeny.this,CountDown.class);
                startActivity(i);
                End();
            }

        });


        Button bn1;
        bn1 = (Button)findViewById(R.id.category_menu);
        bn1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {

                CategoryMenu.GlobalSec = 100;
                Intent i=new Intent(Eredmeny.this,CategoryMenu.class);
                startActivity(i);
                End();
            }

        });


        ListView listView;
        ArrayList<String> listItems;
        ArrayAdapter<String> adapter;


        ListView listView2;
        ArrayList<String> listItems2;
        ArrayAdapter<String> adapter2;



        listView = (ListView) findViewById(R.id.helyesek);
        listItems = new ArrayList<String>();
        listItems.addAll(Eltalat);
        adapter = new ArrayAdapter<String>(this, R.layout.correcttext, listItems);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();




        listView2 = (ListView) findViewById(R.id.rosszak);
        listItems2 = new ArrayList<String>();
        listItems2.addAll(Elrontott);
        adapter2 = new ArrayAdapter<String>(this, R.layout.passtext, listItems2);
        listView2.setAdapter(adapter2);
        adapter2.notifyDataSetChanged();


    }



    public void End()
    {
        Elrontott.clear();
        Eltalat.clear();

        MainActivity.Score = 0;
        this.finish();
        System.gc();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            End();

        }
        else if(keyCode == KeyEvent.KEYCODE_HOME)
        {
            System.exit(1);
        }

        return super.onKeyDown(keyCode, event);
    }


}
