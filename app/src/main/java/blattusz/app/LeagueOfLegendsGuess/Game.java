package blattusz.app.LeagueOfLegendsGuess;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import java.util.Collections;

import static blattusz.app.LeagueOfLegendsGuess.MainActivity.Elrontott;
import static blattusz.app.LeagueOfLegendsGuess.MainActivity.Eltalat;
import static blattusz.app.LeagueOfLegendsGuess.MainActivity.Word;

import android.os.Vibrator;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
public class Game extends Activity implements SensorEventListener {
    Sensor accelometer;
    SensorManager sm;
    TextView accelarion;
    static boolean globalendisactive = false;

    Vibrator vibrator;

    public static boolean muti =  true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        globalendisactive = false;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game);
        Collections.shuffle(Word);
        Collections.shuffle(MainActivity.TaskType);
        CategoryMenu.NewGameGlobalSec = CategoryMenu.GlobalSec;

        MainActivity.VibriOn = true;
        Eltalat.addElement("Correct:");
        Eltalat.addElement("");
        Elrontott.addElement("Pass:");
        Elrontott.addElement("");
muti=true;
        MainActivity.mehet = true;
        MainActivity.mehet2 = true;
        vibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);

        vibrator.vibrate(300);
        Button bn4;
        bn4 = (Button)findViewById(R.id.back);
        bn4.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
               End2();
            }

        });

        if(MainActivity.VibriOn == true)
        {
            vibrator.vibrate(100);
        }



        final Handler h = new Handler();
        h.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {

                CategoryMenu.GlobalSec -= 1;
                TextView Time = (TextView) findViewById(R.id.time);
                Time.setText(""+CategoryMenu.GlobalSec + "");
                TextView Time2 = (TextView) findViewById(R.id.time2);
                Time2.setText(""+CategoryMenu.GlobalSec + "");

                h.postDelayed(this, 1000);



                PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
                boolean isScreenOn = pm.isScreenOn();

                if(isScreenOn == false)
                {
                    End();
                    GlobalEnd();
                    h.removeCallbacks(this);
                }

                if(MainActivity.VibriOn == true)
                {
                    if(CategoryMenu.GlobalSec == 0)
                    {
                        End();
                        vibrator.vibrate(300);
                        h.removeCallbacks(this);
                    }


                }

                if(CategoryMenu.GlobalSec < 1)
                {
                    End();
                    h.removeCallbacks(this);
                }
                if(globalendisactive == true)
                {
                    GlobalEnd();
                    h.removeCallbacks(this);
                }
            }




        }, 1000);



        sm=(SensorManager)getSystemService(SENSOR_SERVICE);
        accelometer=sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sm.registerListener(this,accelometer,SensorManager.SENSOR_DELAY_NORMAL/500);
    }


public void End()
{
    CategoryMenu.GlobalSec = CategoryMenu.NewGameGlobalSec;
    sm.unregisterListener(this);
    Intent i=new Intent(Game.this,Eredmeny.class);
    startActivity(i);
    this.finish();
    System.gc();

}


    public void End2()
    {
        CategoryMenu.GlobalSec = 100;
        sm.unregisterListener(this);
        globalendisactive = true;
        this.finish();
        Elrontott.clear();
        Eltalat.clear();
        Word.clear();
        MainActivity.Score = 0;
        System.gc();
    }


    public void GlobalEnd()
    {
        CategoryMenu.GlobalSec = 100;
        sm.unregisterListener(this);
        globalendisactive = true;
        Intent i=new Intent(Game.this,CategoryMenu.class);
        startActivity(i);
        this.finish();
        Elrontott.clear();
        Eltalat.clear();
        Word.clear();
        MainActivity.Score = 0;
        System.gc();

    }

    @Override
    public void onSensorChanged(final SensorEvent event) {

        Mutasd((int)event.values[0],(int)event.values[1],(int)event.values[2]);
    }
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {}



    public void Mutasd(int x, int y, int z) {


        if(MainActivity.mehet == true)
        {

            if(-MainActivity.erzekenyseg+2 < z  && z< MainActivity.erzekenyseg-2)
            {
                MainActivity.mehet = false;

                if(Word.firstElement().length() > 12)
                {
                    if(CategoryMenu.ischamp == true)
                    {
                        if(muti == true)
                        {
                            muti=false;
                            TextView tvSSID1 = (TextView) findViewById(R.id.textViewSSID);
                            tvSSID1.setTextSize(TypedValue.COMPLEX_UNIT_SP, 60);
                            tvSSID1.setText("" + Word.firstElement());
                            TextView tvSSID2 = (TextView) findViewById(R.id.Type);
                            tvSSID2.setText(MainActivity.TaskType.firstElement());
                        }
                    }
                    else
                    {
                        if(muti == true)
                        {
                            muti=false;
                            TextView tvSSID1 = (TextView) findViewById(R.id.textViewSSID);
                            tvSSID1.setTextSize(TypedValue.COMPLEX_UNIT_SP, 60);
                            tvSSID1.setText("" + Word.firstElement()+ "");
                        }
                    }
                }
                else
                {
                    if(CategoryMenu.ischamp == true)
                    {
                        if(muti == true)
                        {
                            muti=false;
                            TextView tvSSID1 = (TextView) findViewById(R.id.textViewSSID);
                            tvSSID1.setText("" + Word.firstElement());
                            TextView tvSSID2 = (TextView) findViewById(R.id.Type);
                            tvSSID2.setText(MainActivity.TaskType.firstElement());
                        }
                    }
                    else
                    {
                        if(muti == true)
                        {
                            muti=false;
                            TextView tvSSID1 = (TextView) findViewById(R.id.textViewSSID);
                            tvSSID1.setText("" + Word.firstElement());
                        }
                    }
                }
                MainActivity.mehet = true;

            }
            else if(z > MainActivity.erzekenyseg+2 && MainActivity.mehet2 == true)
            {
                Elrontott.addElement(Word.firstElement());
                Word.removeElement(Word.firstElement());
                if(Word.size() == 1)
                {
                    CategoryMenu.GlobalSec = 0;
                }
                else
                {
                    Word.removeElement(Word.firstElement());
                }
                Intent i=new Intent(Game.this,Bad.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i);
                MainActivity.mehet2 = false;
            }
            else if(z < -MainActivity.erzekenyseg-2 && MainActivity.mehet2 == true)
            {
                Eltalat.addElement(Word.firstElement());
                Word.removeElement(Word.firstElement());
                if(Word.size() == 1)
                {
                    CategoryMenu.GlobalSec = 0;
                }
                else
                {
                    Word.removeElement(Word.firstElement());
                }
                Intent i=new Intent(Game.this,Correct.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i);
                MainActivity.mehet2 = false;
            }

        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            CategoryMenu.GlobalSec = 100;
            sm.unregisterListener(this);
            globalendisactive = true;
            this.finish();
            Elrontott.clear();
            Eltalat.clear();
            Word.clear();
            MainActivity.Score = 0;
            System.gc();
        }
        else if(keyCode == KeyEvent.KEYCODE_HOME)
        {
            System.exit(1);
        }

        if(KeyEvent.KEYCODE_POWER == event.getKeyCode()){
            System.exit(1);
        }
        return super.onKeyDown(keyCode, event);
    }


}
