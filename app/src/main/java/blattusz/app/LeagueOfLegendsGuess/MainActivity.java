package blattusz.app.LeagueOfLegendsGuess;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import java.util.Vector;

public class MainActivity extends Activity {

    public static final String PREFS_NAME = "MyPrefsFile";
    public static boolean mehet = false;
    public static boolean mehet2 = false;
public static int Score = 0;
    public static int erzekenyseg = 6;

    private static final String TAG = "MainActivity";



    public static Vector<String> Word = new Vector<>();
    public static Vector<String> Eltalat = new Vector<>();
    public static Vector<String> Elrontott = new Vector<>();
    public static Vector<String> TaskType = new Vector<>();
public static boolean VibriOn = true;
    public static boolean SoundOn = true;




    @Override
    public void onCreate(Bundle savedInstanceState) {


        TaskType.addElement("\n (Explain)");
        TaskType.addElement("\n (Act)");
        TaskType.addElement("\n (Draw)");

        super.onCreate(savedInstanceState);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.activity_main);
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });



        Button bn;
        bn = (Button)findViewById(R.id.bn);
        bn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(MainActivity.this,CategoryMenu.class);
                startActivity(i);

            }

        });



        Button bn2;
        bn2 = (Button)findViewById(R.id.help);
        bn2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(MainActivity.this,Help.class);
                startActivity(i);

            }

        });

        Button donatebn;
        donatebn = (Button) findViewById(R.id.donate);
        donatebn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=2NDW8G7XB8CXS&source=url"));
                startActivity(browserIntent);
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            System.exit(1);

        }
        else if(keyCode == KeyEvent.KEYCODE_HOME)
        {
            System.exit(1);

        }


        return super.onKeyDown(keyCode, event);
    }




}