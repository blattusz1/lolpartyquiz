package blattusz.app.LeagueOfLegendsGuess;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

public class CountDown extends Activity implements SensorEventListener {
    int sec = 6;
    Sensor accelometer;
    SensorManager sm;
    boolean mehet = false;

    private InterstitialAd mInterstitialAd;

boolean stop =false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.countdown);




        MobileAds.initialize(this,
                "ca-app-pub-2285811773346460~5429589723");
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-2285811773346460/8929851039");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                // Load the next interstitial.
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }

        });


        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Log.d("TAG", "The interstitial wasn't loaded yet.");
        }
        Button bn4;
        bn4 = (Button)findViewById(R.id.back);
        bn4.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                stop = true;
                End2();
            }

        });

        stop = false;
        final Handler h = new Handler();
        h.postDelayed(new Runnable()
        {

            @Override
            public void run()
            {
                sec -= 1;
                TextView Time = (TextView) findViewById(R.id.cdtime);
                Time.setText(""+ sec + "");

                h.postDelayed(this, 1000);
                if(sec == 0)
                {
                    mehet = true;
                    h.removeCallbacks(this);
                    SensorStart();

                }
                if(stop == true)
                {
                    h.removeCallbacks(this);
                }
            }

        }, 1000);


    }

    public void End()
    {
        Intent i=new Intent(CountDown.this,Game.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(i);
        this.finish();
    }
    public void SensorStart()
    {
        sm=(SensorManager)getSystemService(SENSOR_SERVICE);
        accelometer=sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sm.registerListener(this,accelometer,SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void End2()
    {
        this.finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            stop = true;

            this.finish();
        }
        else if(keyCode == KeyEvent.KEYCODE_HOME)
        {
            System.exit(1);
        }


        return super.onKeyDown(keyCode, event);
    }



    @Override
    public void onSensorChanged(final SensorEvent event) {

        if(-MainActivity.erzekenyseg+2 < (int)event.values[2]  && (int)event.values[2] < MainActivity.erzekenyseg-2)
        {
            if(mehet == true)
            {
                sm.unregisterListener(this);
                End();
            }


        }
        else
        {
            TextView Time = (TextView) findViewById(R.id.cdtime);
            Time.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30);
            Time.setText("Hold device upright(vertical) to continue!");
        }
    }
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {}



}
