package blattusz.app.LeagueOfLegendsGuess;


import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;

import java.util.Collections;

import static blattusz.app.LeagueOfLegendsGuess.MainActivity.Word;


public class Bad extends Activity implements SensorEventListener{

    int sec = 1;
    boolean mehet = false;
    Sensor accelometer;
    SensorManager sm;
    private AudioManager audio;
    @Override
    protected void onCreate(Bundle savedInstanceState) {



        audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bad);



        mehet = false;


        final Handler h = new Handler();
        h.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                sec -= 1;
                h.postDelayed(this, 1000);
                if(sec < 1)
                {
                    mehet= true;
                    h.removeCallbacks(this);
                }
            }
        }, 1000);

        sm=(SensorManager)getSystemService(SENSOR_SERVICE);
        accelometer=sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sm.registerListener(this,accelometer,SensorManager.SENSOR_DELAY_NORMAL);

        Button bn4;
        bn4 = (Button)findViewById(R.id.back);
        bn4.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                Game.globalendisactive = true;
                End();
            }

        });
    }

    public void End()
    {
        sm.unregisterListener(this);
        this.finish();
        overridePendingTransition(0, 0);
        MainActivity.mehet2 = true;
        Collections.shuffle(Word);
        Collections.shuffle(MainActivity.TaskType);
        Game.muti = true;
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Game.globalendisactive = true;
            End();
        }
        else if(keyCode == KeyEvent.KEYCODE_HOME)
        {
            System.exit(0);
        }

        else if(keyCode == KeyEvent.KEYCODE_VOLUME_UP)
        {
            audio.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_RAISE, AudioManager.FLAG_SHOW_UI);
            return true;
        }

        else if(keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)
        {
            audio.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_LOWER, AudioManager.FLAG_SHOW_UI);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onSensorChanged(final SensorEvent event) {

        if(-MainActivity.erzekenyseg+2 < (int)event.values[2]  && (int)event.values[2] < MainActivity.erzekenyseg-2)
        {
            if(mehet == true)
                End();
        }
    }
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {}

}
