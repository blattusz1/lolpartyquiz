package blattusz.app.LeagueOfLegendsGuess;

import android.app.Activity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.content.Intent;
import android.widget.SeekBar;
import android.widget.TextView;

import blattusz.app.LeagueOfLegendsGuess.Szavak.LoLSzavak;


public class CategoryMenu extends Activity implements SeekBar.OnSeekBarChangeListener {
    public static int GlobalSec = 100;
    public static int NewGameGlobalSec = 100;
    public static boolean ischamp = false;
    private static SeekBar SecBar;
    private TextView SecText;





    @Override
    protected void onCreate(Bundle savedInstanceState) {



        MainActivity.Word.clear();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.category_menu);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        SecText = (TextView)findViewById(R.id.SecProgressId);
        SecText.setText("Sec: 100");




        SecBar = (SeekBar)findViewById(R.id.SecBarId); // make seekbar object
        SecBar.setOnSeekBarChangeListener(this);
        SecBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // TODO Auto-generated method stub
                SecText = (TextView)findViewById(R.id.SecProgressId);
                progress = ((int)Math.round(progress/20))*20;
                seekBar.setProgress(progress);
                if(progress == 0)
                {
                    progress = progress+GlobalSec;
                }
                SecText.setText("Sec: "+progress);
                GlobalSec = progress;
            }
        });



        Button bn;
        MainActivity.mehet = true;
        bn = (Button)findViewById(R.id.Champions);
        bn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(CategoryMenu.this,CountDown.class);
                startActivity(i);
                LoLSzavak.Champions();
                ischamp = true;
                End();
            }

        });

        Button bn1;
        MainActivity.mehet = true;
        bn1 = (Button)findViewById(R.id.Top);
        bn1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(CategoryMenu.this,CountDown.class);
                startActivity(i);
                LoLSzavak.Famous();
                ischamp = false;
                End();
            }

        });

        Button bn2;
        MainActivity.mehet = true;
        bn2 = (Button)findViewById(R.id.Skins);
        bn2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(CategoryMenu.this,CountDown.class);
                startActivity(i);
                LoLSzavak.Skins();
                ischamp = false;
                End();
            }

        });

        Button bn3;
        MainActivity.mehet = true;
        bn3 = (Button)findViewById(R.id.Items);
        bn3.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(CategoryMenu.this,CountDown.class);
                startActivity(i);
                LoLSzavak.Items();
                ischamp = false;
                End();
            }

        });


        Button bn4;
        bn4 = (Button)findViewById(R.id.back);
        bn4.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                End();
            }

        });
    }



    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (seekBar == SecBar)
        {
            SecText.setText("Seconds: Rs "+progress);
        }
        GlobalSec = progress;


    }


    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        // TODO Auto-generated method stub

    }
    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        // TODO Auto-generated method stub

    }



    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            this.finish();

        }
        else if(keyCode == KeyEvent.KEYCODE_HOME)
        {
            System.exit(1);
        }


        return super.onKeyDown(keyCode, event);
    }

    public void End()
    {

        this.finish();
    }


}
